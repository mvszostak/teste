FROM openjdk:8-jre-alpine
ADD helloworld.class helloworld.class
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "helloworld"]
